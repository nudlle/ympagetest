package ru.nudlle.yandex;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;

public class YandexMarketResultPage {
    @Step("Получить список результатов поиска")
    public ElementsCollection getResults() {
        return $$(byXpath("//div[contains(@id,'product')]"));
    }

    public SelenideElement getResult(int index) {
        return $(byXpath("//div[contains(@id,'product')]"), index);
    }

    @Step("Получить название товара по индексу {0}")
    public String getItemTitle(int index) {
        return getResult(index).findElement(By.xpath("//div/h3/a")).getText();
    }

    @Epic("Яндекс.Маркет")
    @Story("Результат поиска")
    @Feature("Проверка первого элемента")
    @Step("Проверка раннее найденного элемента")
    public void checkFirstItem(SelenideElement firstItemElement) {
        assertThat(getResult(0), equalTo(firstItemElement));
    }

    @Epic("Яндекс.Маркет")
    @Story("Результат поиска")
    @Feature("Проверка значений")
    @Step("Проверка количества найденных элементов не меннее чем {0}")
    public void checkResultCountGreaterThan(int count) {
        assertThat(getResults().size(), greaterThan(count));
    }
}
