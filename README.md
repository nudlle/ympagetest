## Тестирование страницы Яндекс.Маркет

#### Для запуска тестов необходимо ввести команду

`./gradlew chrome -Dtimeout=7000`

`./gradlew firefox`

`./gradlew ie`

`./gradlew edge`

`./gradlew safari`

`-Dtimeout` - опционально, таймаут ожидания Selenide, по-умолчанию `10000`

или

`./gradlew.bat chrome
 и т. д.`

#### Для генерации отчёта Аllure

`./gradlew allureReport`

или

`./gradlew.bat allureReport`

#### Вывести отчёт Allure

`./gradlew allureServe`

или

`./gradlew.bat allureServe`

