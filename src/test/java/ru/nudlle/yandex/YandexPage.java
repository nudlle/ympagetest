package ru.nudlle.yandex;

import com.codeborne.selenide.Condition;
import io.qameta.allure.*;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class YandexPage {
    public static int TIMEOUT = 10000;
    @Step("Перейти на страницу Яндекс.Маркет")
    public YandexMarketPage openMarketPage() {
        open("http://yandex.ru");
        $(byXpath("//a[.='Маркет']")).waitUntil(Condition.exist, 10000).click();
        switchTo().window(1);
        return page(YandexMarketPage.class);
    }
}
