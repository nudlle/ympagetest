package ru.nudlle.yandex;

import com.codeborne.selenide.Condition;
import io.qameta.allure.*;

import java.util.Arrays;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class YandexFilterPage {
    @Step("Показать весь список производителей")
    public void showAllManufactures() {
        $(byXpath("//button[@type='button' and span='Показать всё']")).waitUntil(Condition.exist, YandexPage.TIMEOUT).click();
    }

    public void checkManufactures(String...manufactures) {
        showAllManufactures();
        Arrays.stream(manufactures).forEach(manufacture -> {
                    addSearchManufactures(manufacture);
                    checkManufacture(manufacture);
                });
    }

    @Step("Выбор производителя {0}")
    public void checkManufacture(String manufacture) {
        $(byXpath("//label[.='" + manufacture + "']")).should(Condition.exist).should(Condition.enabled).click();
    }

    @Step("Поиск производителя {0}")
    public void addSearchManufactures(String manufactures) {
        $(byXpath("//div[@class='n-filter-block__list n-filter-block__list_type_more more-list__top i-bem n-filter-block__list_js_inited']/span/span/input"))
                .setValue(manufactures)
                .shouldBe(Condition.value(manufactures)).click();
    }
    @Step("Установка значений цены от {0} до {1}")
    public void setUpProductPriceFromTo(String priceFrom, String priceTo) {
        $(byXpath("//input[@id='glf-pricefrom-var']")).should(Condition.exist).setValue(priceFrom)
                .shouldBe(Condition.value(priceFrom)).click();
        $(byXpath("//input[@id='glf-priceto-var']")).should(Condition.exist).setValue(priceTo)
                .shouldBe(Condition.value(priceTo)).click();
    }
    @Step("Применить фильрацию и показать найденное")
    public YandexMarketResultPage clickEnterFilter() {
        $(byXpath("//a[@type='button' and span='Показать подходящие']")).should(Condition.exist).click();
        return page(YandexMarketResultPage.class);
    }
}
