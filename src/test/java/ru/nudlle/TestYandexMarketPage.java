package ru.nudlle;

import com.codeborne.selenide.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.*;
import org.testng.annotations.*;
import ru.nudlle.yandex.YandexFilterPage;
import ru.nudlle.yandex.YandexMarketPage;
import ru.nudlle.yandex.YandexMarketResultPage;
import ru.nudlle.yandex.YandexPage;
import static com.codeborne.selenide.Selenide.*;

public class TestYandexMarketPage {
    @DataProvider(name = "filters")
    public Object[][] getNotebooksFilterData() {
        return new Object[][] {
                {"Компьютеры", "Ноутбуки", "HP", "Lenovo", "", "30000"},
                {"Компьютеры", "Планшеты", "Acer", "DELL", "20000", "25000"}
        };
    }

    @BeforeMethod
    public void setUp() {
        YandexPage.TIMEOUT = Integer.parseInt(System.getProperty("selenide.timeout", "10000"));
        String currentBrowser = System.getProperty("selenide.browser", "chrome");
        if ("chrome".equals(currentBrowser)) {
            WebDriverManager.chromedriver().setup();
        } else if ("firefox".equals(currentBrowser)) {
            WebDriverManager.firefoxdriver().setup();
        } else if ("opera".equals(currentBrowser)) {
            WebDriverManager.operadriver().setup();
        } else if ("edge".equals(currentBrowser)) {
            WebDriverManager.edgedriver().setup();
        } else if ("ie".equals(currentBrowser)) {
            WebDriverManager.iedriver().setup();
        }
        Configuration.startMaximized = true;
    }
    @AfterMethod
    public void tearDown() {
        closeWebDriver();
    }

    @Epics(@Epic("Яндекс.Маркет"))
    @Stories(@Story("Результат поиска"))
    @Features({@Feature("Проверка первого элемента"), @Feature("Проверка значений")})
    @Test(dataProvider = "filters")
    public void testSearchItemsTask(String section, String productType, String manufacture1, String manufacture2, String priceFrom, String priceTo) {
        YandexMarketPage marketPage = new YandexPage().openMarketPage();
        marketPage.clickSection(section);
        marketPage.clickSubsection(productType);
        YandexFilterPage filterPage = marketPage.goToAllFilters();
        filterPage.setUpProductPriceFromTo(priceFrom, priceTo);
        filterPage.checkManufactures(manufacture1, manufacture2);
        YandexMarketResultPage resultPage = filterPage.clickEnterFilter();
        resultPage.checkResultCountGreaterThan(10);
        SelenideElement firstItemElement = resultPage.getResult(0);
        String firstItemTitle = resultPage.getItemTitle(0);
        resultPage = marketPage.searchItem(firstItemTitle);
        resultPage.checkFirstItem(firstItemElement);
    }
}
