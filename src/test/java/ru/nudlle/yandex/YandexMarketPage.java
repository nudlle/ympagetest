package ru.nudlle.yandex;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class YandexMarketPage {
    @Step("Переход в категорию {0}")
    public void clickSection(String section) {
        $(byXpath("//a/span[.='" + section + "']")).waitUntil(Condition.exist, YandexPage.TIMEOUT).click();
    }
    @Step("Переход в подкатегорию {0}")
    public void clickSubsection(String subsection) {
        $(byXpath("//a[.='" + subsection + "']")).waitUntil(Condition.exist, YandexPage.TIMEOUT).click();
    }
    @Step("Переход в расширенный поиск")
    public YandexFilterPage goToAllFilters() {
        $(byXpath("//a/span[.='Все фильтры']")).waitUntil(Condition.exist, YandexPage.TIMEOUT).click();
        return page(YandexFilterPage.class);
    }
    @Step("Поиск товара {0}")
    public YandexMarketResultPage searchItem(String item) {
        $(byXpath("//input[@id='header-search']")).setValue(item).pressEnter();
        return page(YandexMarketResultPage.class);
    }
}
